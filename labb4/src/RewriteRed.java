public class RewriteRed {

  public static void main(String[] args) {
    new RewriteRed();
  }

  private class Edge {
    public int a, b;
  }
  public RewriteRed() {
    Kattio io = new Kattio(System.in, System.out);
    int v = io.getInt();
    int e = io.getInt();
    int m = io.getInt();
    Edge[] edges = new Edge[e];
    for (int i = 1; i <= e; i++) {
      Edge ed = new Edge();
      ed.a = io.getInt();
      ed.b = io.getInt();
      edges[i-1] = ed;
    }
    if (m > v) m = v;

    boolean[] used = new boolean[v];
    int count = 0;
    for (Edge ed : edges) {
      used[ed.a-1] = true;
      used[ed.b-1] = true;
    }

    StringBuilder unu = new StringBuilder();
    for (int i = 0; i < used.length; i++) {
      if (!used[i]) {
        count++;
        unu.append(String.format("2 %d %d\n", i + 4, 3));
      }
    }

    io.printf("%d\n%d\n%d\n", v + 3, e + count + 2, m + 3);
    
    // Extra roller
    io.println("1 1");
    io.println("1 2");
    io.println("1 3");
    
    String roles = genColorRoles(m);
    for (int i = 0; i < v; i++) {
      io.println(roles);
    }
    
    // Base case
    io.println("2 1 3");
    io.println("2 2 3");
    for (Edge ed : edges) {
      io.printf("2 %d %d\n", ed.a + 3, ed.b + 3);
    }

    io.print(unu.toString());
    io.flush();
  }
    
  private String genColorRoles(int m) {
        StringBuilder sb = new StringBuilder();
        sb.append(m).append(' ');
        for (int i = 1; i <= m; i++) {
            sb.append(i+3).append(' ');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
