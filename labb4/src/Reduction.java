public class Reduction {

    public static void main(String[] args) {
        new Reduction();
    }

    // p = m
    // r = e
    // s = v

    public Reduction() {
        Kattio io = new Kattio(System.in, System.out);
        Graph g = readIn(io);
        printOut(io, g);
    }

    public Graph readIn(Kattio io) {
        int v = io.getInt();
        int e = io.getInt();
        int m = io.getInt();
        Edge[] edges = new Edge[e];
        for (int i = 1; i <= e; i++) {
            Edge ed = new Edge();
            ed.a = io.getInt();
            ed.b = io.getInt();
            edges[i-1] = ed;
        }
        if (m > e) e = m;
        return new Graph(v, e, m, edges);
    }

    public void printOut(Kattio io, Graph g) {
        g.used[0] = true;
        g.used[1] = true;
        g.used[2] = true;
        String unused = checkUnused(g);
        io.printf("%d\n%d\n%d\n", g.v + 3, g.e + this.count, g.m + 3); // Roller, Scener, Skådisar = Edges, Verts, Colours
        String l = genColorRoles(g.m);
        for (int n = 1; n <= g.v; n++) {
            io.printf("%s\n", l);
        }
        printDefaultValues(io, g.v, g);
        //io.println("Start unused");
        for (Edge ed : g.edges) {
            io.printf("2 %d %d\n", ed.a + 3, ed.b + 3);
        }
        io.print(unused);
        //io.println("End unused");
        io.flush();
    }

    private int count = 0;

    private String checkUnused(Graph g) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < g.used.length - 1; i++) {
            //sb.append(String.format("Iteration %d\n", i));
            if (!g.used[i]) {
              //sb.append("Not used\n");
              sb.append(String.format("2 %d %d\n", 3, i + 1));
              this.count++;
            }
        }
        return sb.toString();
    }

    private void printDefaultValues(Kattio io, int v, Graph g) {
        io.println("1 1");
        io.println("1 2");
        io.println("1 3");
        io.printf("2 %d %d\n", 1, 3);
        io.printf("2 %d %d\n", 2, 3);
    }

    private String genColorRoles(int m) {
        StringBuilder sb = new StringBuilder();
        sb.append(m).append(' ');
        for (int i = 1; i <= m; i++) {
            sb.append(i+3).append(' ');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    private class Edge {
        public int a, b;
    }

    private class Graph {
        public int v, e, m;
        public Edge[] edges;
        public boolean[] used;
        public Graph(int v, int e, int m, Edge[] edges) {
            this.v = v;
            this.e = e;
            this.m = m;
            this.edges = edges;
            this.used = new boolean[this.v + 3];
            for (Edge ed : this.edges) {
                this.used[ed.a - 1] = true;
                this.used[ed.b - 1] = true;
            }
        }
    }
}
