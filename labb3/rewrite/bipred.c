/**
 * Exempel på in- och utdatahantering för maxflödeslabben i kursen
 * ADK.
 *
 * Använder scanf/printf i C.
 *
 * Author: Per Austrin
 */

#include <stdio.h>
#include <stdlib.h>

// Get(i) -> (edges[i*2], edges[i*2+1])

void readBipartiteGraph(int* edges) {
  int x, y, e;
  // Läs antal hörn och kanter
  scanf("%d%d%d", &x, &y, &e);
  edges = (int*) malloc(sizeof(int)*2*(x+y+e));
  fprintf(stderr, "Malloced edges\n");
  // Läs in kanterna
  int count = 0;
  for (int i = 0; i < e; ++i) {
    int a, b;
    scanf("%d %d", &a, &b);
    edges[count++] = a + 1;
    edges[count++] = b + 1;
  }
  for (int i = 0; i < x; i++) {
    edges[count++] = 1;
    edges[count++] = i + 2;
  }
  for (int i = 0; i < y; i++) {
    edges[count++] = x + i + 2;
    edges[count++] = x + y + 2;
  }
  int v = x + y + 2;
  printf("%d\n%d %d\n%d\n", v, 1, v, x + y + e);
  for (int i = 0; i < x + y + e; ++i) {
    // Kant från u till v med kapacitet c
    printf("%d %d %d\n", edges[i*2], edges[i*2+1], 1);
  }
  // Var noggrann med att flusha utdata när flödesgrafen skrivits ut!
  fflush(stdout);

  // Debugutskrift
  fprintf(stderr, "Skickade iväg flödesgrafen\n");
}


void writeFlowGraph(int* edges, int num_edges, int v) {

  // Skriv ut antal hörn och kanter samt källa och sänka
  printf("%d\n%d %d\n%d\n", v, 1, v, num_edges);
  for (int i = 0; i < num_edges; ++i) {
    // Kant från u till v med kapacitet c
    printf("%d %d %d\n", edges[i*2], edges[i*2+1], 1);
  }
  // Var noggrann med att flusha utdata när flödesgrafen skrivits ut!
  fflush(stdout);

  // Debugutskrift
  fprintf(stderr, "Skickade iväg flödesgrafen\n");
}

// Översätt lösningen på flödesproblemet till en lösning på matchningsproblemet!!!!!!!!!!!!!!
void readMaxFlowSolution() {
  int v, e, s, t, f;

  // Läs in antal hörn, kanter, källa, sänka, och totalt flöde
  // (Antal hörn, källa och sänka borde vara samma som vi i grafen vi
  // skickade iväg)
  scanf("%d%d%d%d%d", &v, &s, &t, &f, &e);

  for (int i = 0; i < e; ++i) {
    int u, v, f;
    // Flöde f från u till v
    scanf("%d%d%d", &u, &v, &f);
  }
}


void writeBipMatchSolution() {
  int x = 17, y = 4711, maxMatch = 0;

  // Skriv ut antal hörn och storleken på matchningen
  printf("%d %d\n%d\n", x, y, maxMatch);

  for (int i = 0; i < maxMatch; ++i) {
    int a, b;
    // Kant mellan a och b ingår i vår matchningslösning
    printf("%d %d\n", a, b);
  }

}


int main(void) {
  int edges;
  int v;
  readBipartiteGraph(&edges);

  //writeFlowGraph(&edges, count, v);

  //readMaxFlowSolution();

  //writeBipMatchSolution();

  // debugutskrift
  fprintf(stderr, "Bipred avslutar\n");
  return 0;
}
