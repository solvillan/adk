/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Lösning för Steg3 av labb3 i ADK DD2350.
 * Författare: R. Doverfelt & W. Ågren
 *
 * Algoritm som hittar maximala matchingen
 * i en bipartit graf.Gör detta genom att översätta
 * problemet till ett maximalt flödesproblem och
 * hittar då det maximala flödet. Det maximala flödet
 * i flödesgrafen är lösningen på vårt matchnings-
 * problem.
 *
 * Använder sig av Kattio för input och output.
 * * * * * * * * * * * * * * * * * * * * * * * * * */
import java.util.*;

public class PartThree {

  public static void main(String[] args) {
    new PartThree();
  }

  public PartThree() {
    Kattio io = new Kattio(System.in);
    BipartIn bin = readBipart(io);
    Node[] g = genGraph(bin);
    int v = bin.x + bin.y + 2;
    int maxFlow = maxFlow(g, 1, v, v);
    LinkedList<Edge> tmp = new LinkedList<>();
    for (Node n : g) {
      for (Edge e : n.edges) {
        if (e.f > 0) {
          tmp.add(e);
        }
      }
    }
    writeBipart(io, bin.x, bin.y, 1, v, maxFlow, tmp);
  }

  public static class Node {
    ArrayList<Edge> edges = new ArrayList<>();
  }

  public static class Edge {
    int u, v, f, c;
    Edge r;

    public Edge(int u, int v, int f, int c) {
      this.u = u;
      this.v = v;
      this.f = f;
      this.c = c;
    }

    public void setRev(Edge e) {
      r = e;
    }
  }

  private Node[] genGraph(BipartIn bin) {
    Node[] g = new Node[bin.x + bin.y + 3];
    for(int i = 0; i < g.length; i++) {
      g[i] = new Node();
    }

    for(Edge ed : bin.edges) {
      Edge a = new Edge(ed.u, ed.v, 0, ed.c);
      Edge b = new Edge(ed.v, ed.u, 0, 0);

      a.setRev(b);
      b.setRev(a);

      g[ed.u].edges.add(a);
      g[ed.v].edges.add(b);

    }
    return g;
  }

  public int maxFlow(Node[] g, int s, int t, int v) {
    int maxFlow = 0;

    while(true) {
      // Array som används för att spara stigen från source till sink [s -> t]
      Edge[] p = new Edge[v+1];
      LinkedList<Node> q = new LinkedList<>();
      q.add(g[s]);

      while(!q.isEmpty()) {
        Node cur = q.remove(0);
        for(Edge e : cur.edges) {
          if(p[e.v] == null && e.v != s && e.c > e.f) {
            p[e.v] = e;
            q.add(g[e.v]);
          }
        }
      }

      if(p[t] == null) {break;}

      int pushFlow = Integer.MAX_VALUE;

      // Hitta maximala flödet som kan användas i den givna stigen genom att hitta
      // det minimala residuala flödet för varje kant i stigen
      for(Edge e = p[t]; e != null; e = p[e.u]) {
        pushFlow = Math.min(pushFlow, e.c - e.f);
      }

      for(Edge e = p[t]; e != null; e = p[e.u]) {
        e.f += pushFlow;
        e.r.f -= pushFlow;
      }

      maxFlow += pushFlow;
    }
    return maxFlow;
  }

  private class BipartIn {
    public int x, y, e;
    public Edge[] edges;

    public BipartIn(int x, int y, int e, Edge[] edges) {
      this.x = x;
      this.y = y;
      this.e = e;
      this.edges = edges;
    }
  }

  private BipartIn readBipart(Kattio io) {
    int x = io.getInt();
    int y = io.getInt();
    int e = io.getInt();

    Edge[] edges = new Edge[e + x + y];

    for (int i = 0; i < e; i++) {
      int a = io.getInt();
      int b = io.getInt();
      edges[i] = new Edge(a + 1, b + 1, 0, 1);
    }

    for (int i = 0; i < x; i++) {
      edges[e + i] = new Edge(1, i + 2, 0, 1);
    }
    for (int i = 0; i < y; i++) {
      edges[e + x + i] = new Edge(x + i + 2, x + y + 2, 0, 1);
    }
    return new BipartIn(x, y, e, edges);
  }

  private void writeBipart(Kattio io, int x, int y, int s, int t, int max, LinkedList<Edge> edges) {
    io.print(String.format("%d %d\n%d\n", x, y, max));

    for (Edge e : edges) {
      if (e.u != s && e.v != t) {
        io.print(String.format("%d %d\n", e.u - 1, e.v - 1));
      }
    }
    io.flush();
  }
}
