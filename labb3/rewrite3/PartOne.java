/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Lösning för Steg1 av labb3 i ADK DD2350.
 * Författare: R. Doverfelt & W. Ågren
 *
 * Använder sig av Kattio för input och output.
 * * * * * * * * * * * * * * * * * * * * * * * * * */
import java.util.*;

public class PartOne {

  public static void main(String[] args) {
    new PartOne();
  }

  public PartOne() {
    Kattio io = new Kattio(System.in);
    BipartIn bin = readBipart(io);
    writeFlow(io, bin.edges, bin.x, bin.y, bin.e);
    FlowIn fin = readFlow(io);
    writeBipart(io, bin.x, bin.y, fin);
  }

  /* Hjälp-klass för att underlätta representering av kanter. */
  private class Edge {
    int u, v, f, c;
    Edge r;

    public Edge(int u, int v, int f, int c) {
      this.u = u;
      this.v = v;
      this.f = f;
      this.c = c;
    }
  }

  /* Hjälp-klass för att göra det möjligt för oss att returnera alla värden vi vill åt. */
  private class BipartIn {
    public int x, y, e;
    public Edge[] edges;

    public BipartIn(int x, int y, int e, Edge[] edges) {
      this.x = x;
      this.y = y;
      this.e = e;
      this.edges = edges;
    }
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Läs in en bipartit graf och gör om den till en flödesgraf där alla kapaciteter är 1.
   * Indata för den bipartita grafen ser ut på följande sätt:
   *
   * X Y
   * |E|
   * u1 v1
   * u2 v2
   * u3 v3
   *   .
   *   .
   *   .
   * ue ve
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  private BipartIn readBipart(Kattio io) {
    int x = io.getInt();
    int y = io.getInt();
    int e = io.getInt();

    Edge[] edges = new Edge[e + x + y];

    for (int i = 0; i < e; i++) {
      int a = io.getInt();
      int b = io.getInt();
      edges[i] = new Edge(a + 1, b + 1, 0, 1);
    }

    for (int i = 0; i < x; i++) {
      edges[e + i] = new Edge(1, i + 2, 0, 1);
    }
    for (int i = 0; i < y; i++) {
      edges[e + x + i] = new Edge(x + i + 2, x + y + 2, 0, 1);
    }
    return new BipartIn(x, y, e, edges);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Utdata för flödesgrafen som transormerats från en bipartit graf ser ut på följande sätt:
   *
   * V
   * S T
   * |E|
   * u1 v1 c1
   * u2 v2 c2
   * u3 v3 c3
   *   .
   *   .
   *   .
   * ue ve ce
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  private void writeFlow(Kattio io, Edge[] edges, int x, int y, int e) {
    int v = x + y + 2;
    io.print(String.format("%d\n%d %d\n%d\n", v, 1, v, x + y + e));

    for (Edge ed : edges) {
      io.print(String.format("%d %d %d\n", ed.u, ed.v, ed.c));
    }
    io.flush();
  }

  private class FlowIn {
    public int s, t, max;
    public Edge[] edges;

    public FlowIn(int s, int t, int max, Edge[] edges) {
      this.s = s;
      this.t = t;
      this.max = max;
      this.edges = edges;
    }
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Residualgrafen i flödesgrafen ser ut på följande sätt:
   *
   * V
   * S T MaxFlow
   * |E|
   * u1 v1 f1
   * u2 v2 f2
   * u3 v3 f3
   *   .
   *   .
   *   .
   * ue ve fe
   * Alla kanter som skrivs ut har positivt flöde.
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  private FlowIn readFlow(Kattio io) {
    int v = io.getInt();
    int s = io.getInt();
    int t = io.getInt();
    int max = io.getInt();
    int e = io.getInt();

    Edge[] edges = new Edge[e];

    for (int i = 0; i < e; i++) {
      int a = io.getInt();
      int b = io.getInt();
      int f = io.getInt();

      edges[i] = new Edge(a, b, f, 0);
    }
    return new FlowIn(s, t, max, edges);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Utdata för den bipartita grafen som transformerats från residualgrafen ser ut på följande sätt:
   *
   * X Y
   * |Em|
   * u1 v1
   * u2 v2
   * u3 v3
   *   .
   *   .
   *   .
   * ue ve
   * Där '|Em|' är antalet kanter i den funna matchningen.
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  private void writeBipart(Kattio io, int x, int y, FlowIn flowIn) {
    io.print(String.format("%d %d\n%d\n", x, y, flowIn.max));

    for (Edge e : flowIn.edges) {
      if (e.u != flowIn.s && e.v != flowIn.t) {
        io.print(String.format("%d %d\n", e.u - 1, e.v - 1));
      }
    }
    io.flush();
  }

}
