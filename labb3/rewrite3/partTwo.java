/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Lösning för Steg2 av labb3 i ADK DD2350.
 * Författare: R. Doverfelt & W. Ågren
 *
 * Algoritm för att hitta maxflödet i godtycklig
 * graf. Använder sig av Edmonds-Karp och BFS,
 * som kan hittas på wikibooks.
 *
 * Använder sig av Kattio för input och output.
 * * * * * * * * * * * * * * * * * * * * * * * * * */
import java.util.*;

public class partTwo {

  public static class Node {
    ArrayList<Edge> edges = new ArrayList<>();
  }

  public static class Edge {
    int u, v, f, c;
    Edge r;

    public Edge(int u, int v, int f, int c) {
      this.u = u;
      this.v = v;
      this.f = f;
      this.c = c;
    }

    public void setRev(Edge e) {
      r = e;
    }
  }

  public int maxFlow(Node[] g, int s, int t, int v) {
    int maxFlow = 0;

    while(true) {
      // Array som används för att spara stigen från source till sink [s -> t]
      Edge[] p = new Edge[v+1];
      LinkedList<Node> q = new LinkedList<>();
      q.add(g[s]);

      while(!q.isEmpty()) {
        Node cur = q.remove(0);
        for(Edge e : cur.edges) {
          if(p[e.v] == null && e.v != s && e.c > e.f) {
            p[e.v] = e;
            q.add(g[e.v]);
          }
        }
      }

      if(p[t] == null) {break;}

      int pushFlow = Integer.MAX_VALUE;

      // Hitta maximala flödet som kan användas i den givna stigen genom att hitta
      // det minimala residuala flödet för varje kant i stigen
      for(Edge e = p[t]; e != null; e = p[e.u]) {
        pushFlow = Math.min(pushFlow, e.c - e.f);
      }

      for(Edge e = p[t]; e != null; e = p[e.u]) {
        e.f += pushFlow;
        e.r.f -= pushFlow;
      }

      maxFlow += pushFlow;
    }
    return maxFlow;
  }

  public static void main(String[] args) {
    Kattio io = new Kattio(System.in);
    partTwo pt = new partTwo();
    int vertices = io.getInt();
    int s = io.getInt();
    int t = io.getInt();
    int edges = io.getInt();

    Node[] g = new Node[vertices+1];
    for(int i = 0; i < g.length; i++) {
      g[i] = new Node();
    }

    for(int i = 0; i < edges; i++) {
      int u = io.getInt();
      int v = io.getInt();
      int c = io.getInt();

      Edge a = new Edge(u, v, 0, c);
      Edge b = new Edge(v, u, 0, 0);

      a.setRev(b);
      b.setRev(a);

      g[u].edges.add(a);
      g[v].edges.add(b);

    }

    int maxFlow = pt.maxFlow(g, s, t, vertices);
    LinkedList<Edge> remain = new LinkedList<>();

    for (Node n : g) {
      for (Edge e : n.edges) {
        if (e.f > 0) {
          remain.add(e);
        }
      }
    }
    io.print(String.format("%d\n%d %d %d\n%d\n", vertices, s, t, maxFlow, remain.size()));
    for (Edge e : remain) {
        io.print(String.format("%d %d %d\n", e.u, e.v, e.f));

    }
    io.flush();
    io.close();
  }
}
