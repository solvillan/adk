use std::cell::{RefCell};
use std::boxed::{Box};
use std::io::{stdin, BufRead};
use std::usize;
use std::cmp::min;

macro_rules! scan {
    ( $string:expr, $sep:expr, $( $x:ty ),+ ) => {{
        let mut iter = $string.split($sep);
        ($(iter.next().and_then(|word| word.parse::<$x>().ok()),)*)
    }}
}

fn main() {
    
}

#[derive(Clone)]
struct Edge {
    u: usize,
    v: usize,
    flow: usize,
    cap: usize,
    reverse: Option<Box<Edge>>
}

impl Edge {

    pub fn new(u: usize, v: usize, flow: usize, cap: usize) -> Edge {
        Edge{u, v, flow, cap, reverse: None}
    }

    pub fn setReverse(&mut self, o: Box<Edge>) {
        self.reverse = Some(o);
    }
}

#[derive(Clone)]
struct Node {
    edges: RefCell<Vec<Edge>>
}

fn getMaxFlow(nodes: RefCell<Vec<RefCell<Node>>>, src: usize, sink: usize) -> usize {
    let mut maxFlow = 0;
    loop {
        let mut parents: Vec<Box<Edge>> = vec![];
        let mut q: Vec<RefCell<Node>> = vec![];
        q.push(*nodes[src]);

        while (q.len() != 0) {
            let cur = q.remove(0);

            for ed in cur.edges.as_slice() {
                if (parents.get(ed.v).is_none() && ed.v != src && ed.cap > ed.flow) {
                    parents[ed.v] = &*ed;
                    q.push(&nodes[ed.v]);
                }
            }
        }

        if parents.get(sink).is_none() {
            break;
        }

        let mut pushFlow = usize::MAX;

        let mut e = parents.get(sink);
        while !e.is_none() {
            let ed = *e.unwrap();
            pushFlow = min(pushFlow, ed.cap - ed.flow);
            e = parents.get(ed.u);
        }
        e = parents.get(sink);
        while !e.is_none() {
            let mut ed = e.as_ref().unwrap();
            ed.flow = ed.flow + pushFlow;
            ed.reverse.as_ref().unwrap().flow = ed.reverse.as_ref().unwrap().flow - pushFlow;
            e = parents.get(ed.u);
        }

        maxFlow = maxFlow + pushFlow;
    }

    return maxFlow;
}

fn readFlow() -> (Vec<RefCell<Node>>, usize, usize) {
    //let wait = Instant::now();
    //eprintln!("Waiting for flowgraph");
    let mut sin = stdin();
    let mut stdin = sin.lock();
    let mut s = String::new();

    let mut nodes: Vec<RefCell<Node>> = vec![];

    stdin.read_line(&mut s);
    //eprintln!("Read first line of flowgraph ({}ms)", wait.elapsed().as_millis());
    //let now = Instant::now();
    let (nv,) = scan!(&s, char::is_whitespace, usize);
    s.clear();
    let v = nv.unwrap_or_default();

    stdin.read_line(&mut s);
    let (ns, nt) = scan!(&s, char::is_whitespace, usize, usize);
    s.clear();
    let start = ns.unwrap_or_default();
    let t = nt.unwrap_or_default();
    
    stdin.read_line(&mut s);
    let (ne,) = scan!(&s, char::is_whitespace, usize);
    s.clear();
    let e = ne.unwrap_or_default();

    for _ in 0..v {
        nodes.push(RefCell::new(Node{edges: RefCell::new(Vec::<Edge>::new())}));
    }

    for i in 0..e {
        stdin.read_line(&mut s);
        let (nu, nv, nw) = scan!(&s, char::is_whitespace, usize, usize, usize);
        s.clear();
        let u = nu.unwrap_or_default();
        let v = nv.unwrap_or_default();
        let w = nw.unwrap_or_default();
        let mut a = RefCell::new(Edge::new(u, v, 0, w));
        let mut b = RefCell::new(Edge::new(v, u, 0, 0));

        a.setReverse(b.clone());
        b.setReverse(a.clone());

        nodes[u].edges.push(a);
        nodes[v].edges.push(b);
    }
    //eprintln!("Done reading flowgraph ({}ms)", now.elapsed().as_millis());
    (nodes, start, t)
}
