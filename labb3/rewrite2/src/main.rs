use std::io::{BufRead, stdin, stdout, Write};
//use std::cmp::min;
use std::usize;
//use std::time::{Instant};

macro_rules! scan {
    ( $string:expr, $sep:expr, $( $x:ty ),+ ) => {{
        let mut iter = $string.split($sep);
        ($(iter.next().and_then(|word| word.parse::<$x>().ok()),)*)
    }}
}

fn main() {
    let (edges, x, y, e) = readBipart();
    //eprintln!("x: {}, y: {}, e: {}\nedges: {:?}", x, y, e, edges);
    writeFlow(&edges, x, y, e);
    let (nedges, s, t, max) = readFlow();
    writeBipart(&nedges, x, y, s, t, max);
}

//#[derive(Debug)]
struct Edge {
    a: usize,
    b: usize,
    w: usize,
}

fn readBipart() -> (Vec<Edge>, usize, usize, usize) {
    //eprintln!("Begin reading Bipart");
    //let now = Instant::now();
    let mut sin = stdin();
    let mut stdin = sin.lock();
    let mut s = String::new();

    let mut edges: Vec<Edge> = vec![];

    stdin.read_line(&mut s);
    let (nx, ny) = scan!(&s, char::is_whitespace, usize, usize);
    s.clear();
    let x = nx.unwrap_or_default();
    let y = ny.unwrap_or_default();
    
    stdin.read_line(&mut s);
    let (ne,) = scan!(&s, char::is_whitespace, usize);
    s.clear();
    let e = ne.unwrap_or_default();

    for i in 0..e {
        stdin.read_line(&mut s);
        let (a, b) = scan!(&s, char::is_whitespace, usize, usize);
        s.clear();
        edges.push(Edge{a: a.unwrap_or_default() + 1, b: b.unwrap_or_default() + 1, w: 1});
    }
    for i in 2..(x+2) {
        edges.push(Edge{a: 1, b: i, w: 1});
    }
    for i in (x+2)..(e+3) {
        edges.push(Edge{a: i, b: x + y + 2, w: 1});
    }
    //eprintln!("Done reading bipart ({}ms)", now.elapsed().as_millis());
    (edges, x, y, e)
}

fn writeFlow(edges: &[Edge], x: usize, y: usize, e: usize) {
    //let now = Instant::now();
    //eprintln!("Begun writing flowgraph");
    let v = x + y + 2;
    let mut tout = stdout();
    let mut out = tout.lock();
    out.write_fmt(format_args!("{}\n{} {}\n{}\n", v, 1, v, x + y + e));

    for edge in edges {
        out.write_fmt(format_args!("{} {} {}\n", edge.a, edge.b, edge.w));
    }
    out.flush().unwrap();
    //eprintln!("Wrote flowgraph ({}ms)", now.elapsed().as_millis());
}

fn readFlow() -> (Vec<Edge>, usize, usize, usize) {
    //let wait = Instant::now();
    //eprintln!("Waiting for flowgraph");
    let mut sin = stdin();
    let mut stdin = sin.lock();
    let mut s = String::new();

    let mut edges: Vec<Edge> = vec![];

    stdin.read_line(&mut s);
    //eprintln!("Read first line of flowgraph ({}ms)", wait.elapsed().as_millis());
    //let now = Instant::now();
    let (nv,) = scan!(&s, char::is_whitespace, usize);
    s.clear();
    let v = nv.unwrap_or_default();

    stdin.read_line(&mut s);
    let (ns, nt, nmax) = scan!(&s, char::is_whitespace, usize, usize, usize);
    s.clear();
    let start = ns.unwrap_or_default();
    let t = nt.unwrap_or_default();
    let max = nmax.unwrap_or_default();
    
    stdin.read_line(&mut s);
    let (ne,) = scan!(&s, char::is_whitespace, usize);
    s.clear();
    let e = ne.unwrap_or_default();

    for i in 0..e {
        stdin.read_line(&mut s);
        let (a, b, w) = scan!(&s, char::is_whitespace, usize, usize, usize);
        s.clear();
        edges.push(Edge{a: a.unwrap_or_default(), b: b.unwrap_or_default(), w: w.unwrap_or_default()});
    }
    //eprintln!("Done reading flowgraph ({}ms)", now.elapsed().as_millis());
    (edges, start, t, max)
}

fn writeBipart(edges: &[Edge], x: usize, y: usize, s: usize, t: usize, max: usize) {
    //let now = Instant::now();
    //eprintln!("Begin writing bipart");
    let mut tout = stdout();
    let mut out = tout.lock();
    out.write_fmt(format_args!("{} {}\n{}\n", x, y, max));
    let remain: Vec<Edge> = edges.iter().filter(|ed| (ed.a != s && ed.b != t))
        .map(|ed| Edge{a: ed.a-1, b: ed.b-1, w: ed.w}).collect();
    for i in 0..max {
        out.write_fmt(format_args!("{} {}\n", remain[i].a, remain[i].b));
    }
    out.flush().unwrap();
    //eprintln!("Done writing bipart ({}ms)", now.elapsed().as_millis());
}
