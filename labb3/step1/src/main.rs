/*
En bipartit graf består av två mängder, X & Y.
Om man angett a hörn i X och b hörn iY så låter vi X = {1,2,3,...,a} och Y = {a+1,a+2,a+3,...,a+b}
*/

use std::io::{BufRead, stdin};
use std::cmp::min;
use std::usize;
use step1::graph::Graph;


macro_rules! scan {
    ( $string:expr, $sep:expr, $( $x:ty ),+ ) => {{
        let mut iter = $string.split($sep);
        ($(iter.next().and_then(|word| word.parse::<$x>().ok()),)*)
    }}
}

fn main() {
    let (mut g, s, t) = readin();
    println!("--== Graph  G==--");
    g.pretty_print();
    let (cf, f) = fordFulk(&mut g, s, t);
    println!("--== Graph CF==--");
    cf.pretty_print();
    println!("--== Graph  F==--");
    f.pretty_print();
}

fn fordFulk(c: &mut Graph, s: usize, t: usize) -> (Graph, Graph) {
    let mut f = Graph::new(c.v);
    let mut cf = Graph::new(c.v);
    let mut c2 = Graph::new(c.v);
    for (u, v) in c.edges() {
        eprintln!("Edge: ({}, {})", u, v);
        f.insert_edge(u, v, 0);
        f.insert_edge(v, u, 0);
        cf.insert_edge(u, v, c.get_weight(u, v).unwrap_or(0));
        cf.insert_edge(v, u, c.get_weight(v, u).unwrap_or(0));
        c2.insert_edge(u, v, c.get_weight(u, v).unwrap_or(0));
    }
    c2.pad();
    println!("--== F ==--");
    f.pretty_print();
    println!("--== CF ==--");
    cf.pretty_print();
    loop {
        let pimp = c2.dijkstra(s, t);
        let mut p: Vec<(usize, usize)> = Vec::new();
        match pimp {
            Some(a) => {
                p = a;
            },
            None => {
                break;
            }
        }
        eprintln!("Dijkstra: {:?}", p);
        for (u, v) in p {
            eprintln!("Do: ({}, {})", u, v);
            eprintln!("Weight({}, {}) = {:?}", u, v, c2.get_weight(u, v));
            let r = min(cf.get_weight(u, v).unwrap_or(usize::MAX), c2.get_weight(u, v).unwrap_or(usize::MAX));
            f.change_weight(u, v, f.get_weight(u, v).unwrap() + r);
            f.change_weight(v, u, f.get_weight(u, v).unwrap());
            cf.change_weight(u, v, c2.get_weight(u, v).unwrap_or(usize::MAX) - f.get_weight(u, v).unwrap());
            cf.change_weight(v, u, c2.get_weight(v, u).unwrap_or(usize::MAX) - f.get_weight(v, u).unwrap());
        }
    }
    return (cf, f);
}

// (Graph, s, t)
fn readin() -> (Graph, usize, usize) {
    let mut sin = stdin();
    let mut stdin = sin.lock();
    let mut s = String::new();

    stdin.read_line(&mut s);
    let (numv,) = scan!(&s, char::is_whitespace, usize);
    s.clear();
    let mut g = Graph::new(numv.unwrap());

    stdin.read_line(&mut s);
    let (vs, vt) = scan!(&s, char::is_whitespace, usize, usize);
    s.clear();

    stdin.read_line(&mut s);
    let (c,) = scan!(&s, char::is_whitespace, usize);
    for _ in 0..c.unwrap() {
        s.clear();
        stdin.read_line(&mut s);
        let (va, vb, weight) = scan!(&s, char::is_whitespace, usize, usize, usize);
        g.insert_edge(va.unwrap() - 1, vb.unwrap() - 1, weight.unwrap());
    }
    return (g, vs.unwrap() - 1, vt.unwrap() - 1);
}
