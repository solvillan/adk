#!/bin/bash

echo "=> Tokenize & build index..."
./tok < /afs/nada.kth.se/info/adk19/labb1/korpus | ./index > /var/tmp/index.txt

echo "=> Build HashMap..."
./konk --build 2>&1 > /dev/null

echo "=> Done."
