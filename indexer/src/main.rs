use std::collections::HashMap;
use std::collections::hash_map::{Entry};
use std::io;
use std::io::{Read, Write, stdin, Stdin, BufRead, stderr};
use std::mem::transmute;
use std::env;

fn main() {
    let words = readTokens();
    let args: Vec<String> = env::args().collect();

    writeText(words);
}

fn writeText(words: HashMap<Box<[u8]>, Vec<u64>>) {
    let mut items: Vec<_> = words.iter().collect(); //Vektor av tupler - [{key, value},...]
    items.sort_unstable_by_key(|k| k.0); //Sortera efter key
    for (k, v) in items {
        io::stdout().write(&*k).expect("Error writing word!");
        print!(":");
        for val in v.as_slice() { //För varje värde i v, som är en vektor av index, skriv ut dem.
            //Gör oxå om vektorn till en slice så vi kan iterera igenom den. Inte längre Dynamisk
            print!("{:X},", val); //Printa val i hexadecimalt
        }
        println!("");
    }
    io::stdout().flush().expect("Error flushing!");
}

fn writeBin(words: HashMap<Box<[u8]>, Vec<u64>>) {
    let mut items: Vec<_> = words.iter().collect();
    items.sort_unstable_by_key(|k| k.0);
    for (k, v) in items {
        io::stdout().write(&*k).expect("Error writing word!");
        io::stdout().write(&[0x00]).expect("Error writing NUL");
        let len: [u8; 2] = unsafe { transmute((v.len() as u16).to_be()) };
        io::stdout().write(&len).expect("Error writing Length");
        for val in v.as_slice() {
            let bytes: [u8; 8] = unsafe { transmute(val.to_be()) };
            io::stdout().write(&bytes).expect("Error writng pos");
        }
    }
}

fn readTokens() -> HashMap<Box<[u8]>, Vec<u64>> {
    let mut buf: Vec<u8> = vec![];
    let mut words: HashMap<Box<[u8]>, Vec<u64>> = HashMap::new();
    for b in stdin().bytes() {
        let byte: u8 = b.unwrap();
        if byte == LF {
            let mut word: Vec<u8> = vec![];
            let mut numb: Vec<u8> = vec![];
            buf.reverse();
            loop {
                let c = buf.pop().unwrap();
                if c != SP {
                    word.push(c);
                } else {
                    break;
                }
            }
            loop {
                match buf.pop() {
                    Some(c) => {
                        numb.push(c);
                    },
                    None => {
                        break;
                    }
                }
            }
            let tmp = word.into_boxed_slice();
            match words.entry(tmp) {
                Entry::Occupied(mut entry) => {
                    entry.get_mut().push(dec_to_u64(&mut numb)); //Om ordet redan är hashat, lägg till positionen för ordet i vektorn
                },
                Entry::Vacant(entry) => {
                    entry.insert(vec![dec_to_u64(&mut numb)]); //Ordet inte hashat, skapa en vektor med positioner och lägg till positionen.
                }
            }
            buf = vec![];
        } else {
            buf.push(byte)
        }
    }
    return words;
}

fn dec_to_u64(data: &mut Vec<u8>) -> u64 {
    let mut tmp = 0u64;
    data.reverse();
    for (p, c) in data.iter().enumerate() {
        if (0x30 <= *c || *c <= 0x39) {
            tmp += (*c as u64 - 0x30) * (10u64).pow(p as u32);
        }
    }
    return tmp;
}

const SP: u8 = 0x20;
const LF: u8 = 0x0A;
const CR: u8 = 0x0D;

fn readIn<'a>() -> HashMap<Box<[u8]>, Vec<u64>> {
    let mut words: HashMap<Box<[u8]>, Vec<u64>> = HashMap::new();
    let mut offset: usize = 0;
    let mut inp = stdin();
    loop {
        match readWord(&mut inp) {
            Some(word) => {
                if word.len() <= 0 {
                    offset += 1;
                    continue;
                } else {
                    let len = word.len() + 1;
                    stderr().write(&word).expect("Error");
                    eprintln!(" - Offset: {}", len + offset);
                    let tmp = word.into_boxed_slice();
                    match words.entry(tmp) {
                        Entry::Occupied(mut entry) => {
                            entry.get_mut().push((offset) as u64); //Om ordet redan är hashat, lägg till positionen för ordet i vektorn
                        },
                        Entry::Vacant(entry) => {
                            entry.insert(vec![(offset) as u64]); //Ordet inte hashat, skapa en vektor med positioner och lägg till positionen.
                        }
                    }
                    offset += len; // 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16
                }

            },
            None => { //Vi är klara
                eprintln!("EOF");
                break;
            }
        }
    }
    return words;
}

fn readWord(inp:&mut Stdin) -> Option<Vec<u8>> {
    let mut word = vec![];
    let mut found: bool = false;
    loop {
        let mut c: [u8; 1] = [0];
        let res = inp.read(&mut c);
        if res.unwrap() == 0 {break;}
        found = true;
        if c[0] == SP || c[0] == CR || c[0] == LF {break;}
        word.push(c[0]);
    }
    word.retain(|&c| (c > 0x40 && c < 0x5B) || (c > 0x60 && c < 0x7B) || (c >= 0xC0 && c <= 0xFF));
    word = word.iter().map(|&c| to_lower(c)).collect::<Vec<_>>();
    if found {
        Some(word)
    } else {
        None
    }
}

fn to_lower(c: u8) -> u8 {
    if (c > 0x40 && c < 0x5B) || (c >= 0xC0 && c <= 0xDE) {
        return c + 0x20;
    } else {
        return c;
    }
}
