use std::env;
use std::fs::File;
use std::io::*;
use std::ffi::OsString;
use konkordans::lazyhash::LazyHash;
use std::mem::transmute;
use std::os::unix::ffi::{OsStrExt, OsStringExt};
use std::time::Instant;

fn main() {
    let args: Vec<_> = env::args_os().collect();
    if(args.len() < 2){
        println!("No given argument!");
        return;
    }
    if(args[1].as_os_str().as_bytes() == b"--build") {
        let mut hash: LazyHash = LazyHash::new();
        readIndex(&mut hash);
        hash.writeToFile().expect("Could not write to file");
    } else {
        let mut hash: LazyHash = LazyHash::fromFile().expect("Could not read from file");
        let mut arg: Vec<u8> = vec![];
        for b in args[1].as_os_str().as_bytes() {
            arg.push(to_lower(*b));
        }

        while arg.len() < 3 {
            arg.push(0x00);
        }
        findWord(&hash, &arg);
    }
}

fn askUser() -> bool {
    println!("There are more than 25 occurances of your word, do you wish to print all of them?");
    for co in stdin().lock().bytes() {
        let c = co.unwrap();
        match c {
            0x79 => {return true;}
            0x6E => {return false;}
            _ => {
                println!("Unknown command");
                return false;
            }
        }
    }
    return false;
}

fn readKorp(pos_raw:&mut Vec<Vec<u8>>, word: &Vec<u8>) {
    let mut korp = File::open("/afs/nada.kth.se/info/adk19/labb1/korpus").expect("Could not open file");
    let mut cnt = 0u64;
    let mut cntWords = 0u64;
    let mut askedUser = false;
    let mut pos: Vec<u64> = vec![];
    for iraw in pos_raw.as_slice() {
        pos.push(to_u64(&mut iraw.to_vec()));
    }
    for index in pos {
        if !askedUser && cntWords > 24 {
            if !askUser() {
                askedUser = true;
                break;
            }
            else {
                askedUser = true;
            }
        }
        korp.seek(SeekFrom::Start(index - (32u64 - (word.len() as u64)/2)));
        let mut buf: [u8; 64] = [0; 64];
        korp.read(&mut buf);
        for i in 0..buf.len() {
            if buf[i] == 0x0A {
                buf[i] = 0x20;
            }
        }
        stdout().write(&buf).expect("Error writing");
        cntWords += 1;
        println!("");
    }
    stdout().flush().expect("Error flushing")
}

fn findWord(hash: &LazyHash, word: &Vec<u8>) {
    let v = hash.get(Vec::from(word.as_slice())).expect("Could not find word");
    let mut file = File::open("/var/tmp/index.txt").expect("Could not open file");
    let mut pos: Vec<Vec<u8>> = vec![];
    let now = Instant::now();
    file.seek(SeekFrom::Start(v));
    let mut buf_file = BufReader::new(file);
    let mut to_use = false;
    loop {
        let mut tmp: Vec<u8> = vec![];
        let mut buf: Vec<u8> = vec![];
        let res = buf_file.read_until(0x0A, &mut buf).unwrap();
        if res == 0 {
            break;
        }
        for i in buf {
            if i == 0x3A {
                if tmp.len() > 0 {
                    while tmp.len() < 3 {
                        tmp.push(0x00);
                    }
                    if equalsFull(&tmp, &word) {
                        to_use = true;
                    } else {
                        to_use = false;
                        break;
                    }
                }
                tmp = vec![];
                continue;
            }
            if i == 0x0A {
                if to_use {
                    if tmp.len() > 0 {pos.push(tmp);}
                    break;
                } else {
                    tmp = vec![];
                    continue;
                }
            }
            if i == 0x2C {
                if to_use {pos.push(tmp);}
                tmp = vec![];
                continue;
            }
            tmp.push(i);
        }
        if to_use {break;}
    }
    println!("There are {} occurances of the word. The search took {}ms.", pos.len(), now.elapsed().as_millis());
    readKorp(&mut pos, &word);
}

fn readIndex(hash: &mut LazyHash) {
    let mut index = File::open("/var/tmp/index.txt").expect("Could not open file");
    let file_len = index.metadata().unwrap().len();
    let mut index_buf = BufReader::new(index);
    let mut offset = 0u64;
    let mut last: Vec<u8> = vec![0x00, 0x00, 0x00];
    while file_len > offset {
        let mut buf: Vec<u8> = vec![];
        //readLine(&index, &mut buf);
        index_buf.read_until(0x0A, &mut buf);
        buf.retain(|&c| c != 0x0A);
        let mut word: Vec<u8> = vec![];
        for b in &buf {
            if *b != 0x3A {
                word.push(*b);
            } else {
                break;
            }
        }
        while word.len() < 3 {
            word.push(0x00);
        }
        if !equals(&word, &last) {
            last = word.clone();
            hash.insert(word, offset);
        }
        offset = offset + buf.len() as u64 + 1;
    }
}

fn equals(a: &[u8], b: &[u8]) -> bool {
    if b.len() < 3 || a.len() < 3 {return false;}
    for i in 0..3 {
        if a[i] != b[i] {return false;}
    }
    return true;
}

fn equalsFull(a: &[u8], b: &[u8]) -> bool {
    if b.len() != a.len() {return false;}
    for i in 0..b.len() {
        if a[i] != b[i] {return false;}
    }
    return true;
}

fn to_u64(data: &mut Vec<u8>) -> u64 {
    let mut tmp = 0u64;
    let string = String::from_utf8(data.to_vec()).unwrap();
    tmp = u64::from_str_radix(&string, 16).expect("Error parsing number");
    return tmp;
}

fn dec_to_u64(data: &mut Vec<u8>) -> u64 {
    let mut tmp = 0u64;
    data.reverse();
    for (p, c) in data.iter().enumerate() {
        if (0x30 <= *c || *c <= 0x39) {
            tmp += (*c as u64 - 0x30) * (10u64).pow(p as u32);
        }
    }
    return tmp;
}

fn to_lower(c: u8) -> u8 {
    if (c > 0x40 && c < 0x5B) || (c >= 0xC0 && c <= 0xDE) {
        return c + 0x20;
    } else {
        return c;
    }
}

fn readLine(file: &File, buf: &mut Vec<u8>) {
    for (pos, co) in file.bytes().enumerate() {
        let c = co.unwrap();
        if c != 0x0A {
            buf.push(c);
        } else {
            break;
        }
    }
}
