use std::mem::{transmute, zeroed};
use std::fmt;
use std::slice::Iter;
use std::fs::File;
use std::io::*;

pub struct LazyHash {
    values: [Vec<Value> ; 27000],
}


impl fmt::Debug for LazyHash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for a in self.values.iter() {
            write!(f, "{:?}\n", a);
        }
        Ok(())
    }
}

#[derive(Debug)]
struct Value {
    key: [u8 ; 3],
    val: u64,
}

impl LazyHash {
    pub fn new<>() -> LazyHash {
        return unsafe {LazyHash{values: zeroed()}}
    }

    pub fn insert(&mut self, key: Vec<u8>, val: u64) {
        let h = hash(&key[0..3]);
        let mut k: [u8 ; 3] = Default::default();
        k.copy_from_slice(&key[0..3]);
        self.values[h].push(Value{key: k, val: val});
    }

    pub fn get(&self, key: Vec<u8>) -> Option<u64> {
        let h = hash(&key[0..3]);
        let vals = &self.values[h];
        if(vals.len() == 0) {return None;}
        if(vals.len() == 1) {return Some(vals[0].val);}
        for val in vals {
            if(val.key == key.as_slice()) {return Some(val.val)};
        }
        return None;
    }

    // Map: 4 bytes index | 1 byte num vals | x bytes values
    // Value: 3 bytes key | 8 bytes pos
    pub fn writeToFile(&self) -> std::io::Result<()> {
        let mut hashBin = File::create("/var/tmp/hash.bin").expect("Error opening hash.bin");
        for i in 0..self.values.len() {
            let slot: &Vec<Value> = &self.values[i];
            if slot.len() > 0 {
                let index: [u8; 4] = unsafe {transmute((i as u32).to_be())};
                let size: [u8; 1] = unsafe {transmute(slot.len() as u8)};
                hashBin.write(&index)?;
                hashBin.write(&size)?;
                for val in slot.as_slice() {
                    let key: [u8 ; 3] = val.key;
                    let pos: [u8 ; 8] = unsafe {transmute((val.val).to_be())};
                    hashBin.write(&key)?;
                    hashBin.write(&pos)?;
                }
            }
        }
        Ok(())
    }

    pub fn fromFile() -> Result<LazyHash> {
        let mut tmp = LazyHash::new();
        let mut hashBin = File::open("/var/tmp/hash.bin").expect("Error opening hash.bin");
        loop {
            let mut indexb: [u8; 4] = [0; 4];
            let bread = hashBin.read(&mut indexb);
            if bread.unwrap() == 0 {break;}
            let index = u32::from_be_bytes(indexb);
            let mut numb: [u8; 1] = [0; 1];
            hashBin.read(&mut numb);
            //eprintln!("Num: {}, Index: {}", numb[0], index as usize);
            for i in 0..(numb[0]) {
                //eprint!("+");
                let mut key: [u8; 3] = [0; 3];
                hashBin.read(&mut key);
                let mut posb: [u8; 8] = [0; 8];
                hashBin.read(&mut posb);
                let pos: u64 = u64::from_be_bytes(posb);
                tmp.values[index as usize].push(Value{key: key, val: pos});
            }
            //eprintln!("");
        }
        Ok(tmp)
    }
}

fn hash(key: &[u8]) -> usize {
    return (key[0] as usize * 900 + key[1] as usize * 30 + key[2] as usize) % 27000 as usize;
}
