#!/bin/bash

# Build indexer
echo "Compiling indexer..."
cd indexer
cargo build --release > /dev/null
cp ./target/release/indexer ../index

# Build konkordans
echo "Compiling konkordans..."
cd ../konkordans
cargo build --release > /dev/null
cp ./target/release/konkordans ../konk

# Build tokenizer
echo "Compiling tokenizer..."
cd ..
gcc tokenizer.c -o tok > /dev/null

echo "Compilation done."
