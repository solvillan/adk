use std::io::{Read, Write, stdin, stdout};
use std::convert::TryInto;

fn main() {
    read();
}

fn read() {
    let word = read_word();
    stdout().write(&word).expect("Could not write!");
    println!("");
    let positions = read_pos();
    for (n, pos) in positions.iter().enumerate() {
        println!("Pos {}: {}", n, pos);
    }
    println!("Bytes read: {}", word.len() + 3 + 8 * positions.len());
}

fn read_word() -> Vec<u8> {
    //Dynamisk array
    let mut word: Vec<u8> = vec![];

    //B är ett resultat, antingen Ok(value) | Err(" godtyckligt error message")
    for (_pos, b) in stdin().lock().bytes().enumerate() {
        let byte: u8 = b.unwrap(); //Unwrap antar att det är ok
        eprintln!("Read word {:02X}", byte); //Printa på error streamen
        if byte == 0x00 {
            break;
        }
        word.push(byte);
    }
    return word;
}

fn read_pos() -> Vec<u64> {
    let mut positions: Vec<u64> = vec![];

    let mut len_buf: [u8; 2] = [0; 2];
    stdin().lock().read(&mut len_buf).expect("Could not read pos_len");
    eprintln!("Read len: {:02X} {:02X}", len_buf[0], len_buf[1]);
    let pos_len = u16::from_be_bytes(len_buf) as usize;
    eprintln!("Num pos: {}", pos_len);

    let mut pos_buf: Vec<u8> = vec![];
    for (pos, b) in stdin().lock().bytes().enumerate() {
        if pos < pos_len * 8 {
            pos_buf.push(b.unwrap());
            eprintln!("Read pos byte: {:02X}", pos_buf.last().expect("Empty buffer"));
        } else {
            break;
        }
    }

    for i in 0..pos_len {
        let p_buf: [u8; 8] = pos_buf[(i*8)..(i*8+8)].try_into().expect("Not enough bytes!");
        let p: u64 = u64::from_be_bytes(p_buf);
        positions.push(p);
    }

    return positions;
}
