import java.util.*;

public class Heuristik2 {

    private static ArrayList<HashSet<Integer>> roles, scenes;
    private static HashMap<Integer, HashSet<Integer>> possibleRoles, actorMap;
    private static int actorCount = 0;
    private static HashSet<Integer> unsetRoles = new HashSet<>(), takenRoles = new HashSet<>();

    public static void main(String[] args) {
        Kattio io = new Kattio(System.in, System.out);
        int n = io.getInt();
        int s = io.getInt();
        int k = io.getInt();

        roles = new ArrayList<>();
        scenes = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int c = io.getInt();
            HashSet<Integer> acts = new HashSet<>();
            for (int j = 0; j < c; j++) {
                acts.add(io.getInt());
            }
            roles.add(acts);
        }
        for (int i = 0; i < s; i++) {
            int c = io.getInt();
            HashSet<Integer> role = new HashSet<>();
            for (int j = 0; j < c; j++) {
                role.add(io.getInt());
            }
            scenes.add(role);
        }

        possibleRoles = new HashMap<>();
        actorMap = new HashMap<>();

        for (int i = 1; i <= n; i++) {
            HashSet<Integer> acts = roles.get(i-1);
            for (int act : acts) {
                if (!possibleRoles.containsKey(act)) {
                    possibleRoles.put(act, new HashSet<>());
                }
                possibleRoles.get(act).add(i);
            }
            unsetRoles.add(i);
        }

        setDivas();
        setActors(k);
        setSupers(k);

        printOut(io);
        io.close();
    }

    private static void printOut(Kattio io) {
        io.printf("%d\n", actorCount);
        for (Map.Entry<Integer, HashSet<Integer>> ent : actorMap.entrySet()) {
            StringBuilder sb = new StringBuilder();
            for (int role : ent.getValue()) {
                sb.append(role).append(' ');
            }
            io.printf("%d %d %s\n", ent.getKey(), ent.getValue().size(), sb.toString());
        }
        io.flush();
    }

    private static void setSupers(int k) {
        int cur = k + 1;
        Iterator<Integer> it = unsetRoles.iterator();

        while (it.hasNext()) {
            addSuper(cur++, it.next());
            it.remove();
        }
    }

    private static void addSuper(int act, int role) {
        if (!actorMap.containsKey(act)) {
            actorMap.put(act, new HashSet<>());
            actorCount++;
        }
        actorMap.get(act).add(role);
        takenRoles.add(role);
    }

    private static void setActors(int k) {
        for (int act = 3; act <= k; act++) {
            HashSet<Integer> roles = possibleRoles.get(act);
            for (int role : roles) {
                if (!takenRoles.contains(role) && canPlay(act, role)) {
                    addActor(act, role);
                }
            }
        }
    }

    private static boolean canPlay(int act, int role) {
        if (!actorMap.containsKey(act)) return true;

        for (HashSet<Integer> scene : scenes) {
            for (int r : actorMap.get(act)) {
                if (scene.contains(r) && scene.contains(role)) return false;
            }
        }

        return true;
    }

    private static void addActor(int act, int role) {
        if (!actorMap.containsKey(act)) {
            actorMap.put(act, new HashSet<>());
            actorCount++;
        }
        actorMap.get(act).add(role);
        unsetRoles.remove(role);
        takenRoles.add(role);
    }

    private static void setDivas() {
        HashSet<Integer> d1 = possibleRoles.get(1), d2 = possibleRoles.get(2);
        actorMap.put(1, new HashSet<>());
        actorMap.put(2, new HashSet<>());
        actorCount = 2;

        for (int r1 : d1) {
            for (int r2 : d2) {
                if (r1 == r2) continue;
                for (HashSet<Integer> scene : scenes) {
                    if (scene.contains(r1) && scene.contains(r2)) break;
                    if (isValidDiva(r1, r2)) {
                        addActor(1, r1);
                        addActor(2, r2);
                        return;
                    }
                }
            }
        }
    }

    private static boolean isValidDiva(int r1, int r2) {
        for (HashSet<Integer> scene : scenes) {
            if (scene.contains(r1) && scene.contains(r2)) return false;
        }
        return true;
    }

}
