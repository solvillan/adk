import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;

public class Verifier {
    final static Kattio io = new Kattio(System.in, System.out);

    enum STATUS_CODE {
        VALID("Solution OK."), NON_VALID("Solution not OK."), ACTOR_ALONE("Solution not OK. Monologue exists."),
        DIVA_MISSING("Solution not OK. Diva not assigned."), ROLE_CONFLICT("Solution not OK. Role conflict."),
        DIVA_CONFLICT("Solution not OK. Diva conflict."), DIVA_NO_SCENE("Solution not OK. Diva missing scene."),
        UNKNOWN_ERROR("Unknown state.");

        private String msg;

        STATUS_CODE(String msg) {
            this.msg = msg;
        }

        public String toString() {
            return this.name() + ": " + this.msg;
        }
    }

    public static void main(final String[] args) {
        switch (verifySolution()) {
        case VALID:
            System.out.println(STATUS_CODE.VALID);
            break;
        case NON_VALID:
            System.out.println(STATUS_CODE.NON_VALID);
            break;
        case ACTOR_ALONE:
            System.out.println(STATUS_CODE.ACTOR_ALONE);
            break;
        case DIVA_MISSING:
            System.out.println(STATUS_CODE.DIVA_MISSING);
            break;
        case ROLE_CONFLICT:
            System.out.println(STATUS_CODE.ROLE_CONFLICT);
            break;
        case DIVA_CONFLICT:
            System.out.println(STATUS_CODE.DIVA_CONFLICT);
            break;
        default:
            System.out.println(STATUS_CODE.UNKNOWN_ERROR);
            break;
        }

        io.close();
    }

    static STATUS_CODE verifySolution() {
        final int n = io.getInt();
        final int s = io.getInt();
        io.getInt(); // Read K, but discard.

        @SuppressWarnings("unchecked")
        final HashSet<Integer>[] roles = new HashSet[n + 1];
        @SuppressWarnings("unchecked")
        final HashSet<Integer>[] scenes = new HashSet[s + 1];

        for (int i = 1; i <= n; i++) {
            final int count = io.getInt();
            roles[i] = new HashSet<>();
            for (int j = 0; j < count; j++) {
                roles[i].add(io.getInt());
            }
        }

        scenes[0] = new HashSet<>();
        for (int i = 0; i < s; i++) {
            final int count = io.getInt();
            scenes[i + 1] = new HashSet<>();
            for (int j = 0; j < count; j++) {
                scenes[i + 1].add(io.getInt());
            }
        }

        final int activeActors = io.getInt();
        final HashMap<Integer, ArrayList<Integer>> setRoles = new HashMap<>();

        for (int i = 0; i <= n; i++) {
            setRoles.put(i, new ArrayList<Integer>());
        }

        boolean d1 = false, d2 = false;
        for (int i = 0; i < activeActors; i++) {
            final int actor = io.getInt();
            if (actor == 1) d1 = true;
            if (actor == 2) d2 = true;
            final int count = io.getInt();

            for (int j = 0; j < count; j++) {
                final int role = io.getInt();
                setRoles.get(role).add(actor);
            }
        }
        if (!(d1 && d2)) return STATUS_CODE.DIVA_MISSING;

        final HashSet<Integer> setActors = new HashSet<>();

        d1 = false;
        d2 = false;
        for (final HashSet<Integer> scene : scenes) {
            final HashSet<Integer> roleActors = new HashSet<>();
            boolean diva1 = false;
            boolean diva2 = false;

            if (scene.size() == 1) {
                return (STATUS_CODE.ACTOR_ALONE);
            }

            for (final int role : scene) {
                if (setRoles.get(role).size() != 1) {
                    return (STATUS_CODE.ROLE_CONFLICT);
                }

                final int actor = setRoles.get(role).get(0);

                if (actor == 1) {
                    d1 = true;
                    diva1 = true;
                } else if (actor == 2) {
                    d2 = true;
                    diva2 = true;
                }

                if (diva1 && diva2) {
                    return (STATUS_CODE.DIVA_CONFLICT);
                }

                if (roleActors.contains(actor)) {
                    return (STATUS_CODE.ACTOR_ALONE);
                } else {
                    roleActors.add(actor);
                    setActors.add(actor);
                }
            }
            roleActors.clear();
        }

        if (!d1 || !d2) return STATUS_CODE.DIVA_NO_SCENE;
        STATUS_CODE res = (activeActors == setActors.size()) ? STATUS_CODE.VALID : STATUS_CODE.NON_VALID;
        return (res);
    }
}