import java.util.*;
/*
*   Labb5 lösning, senast redovisad och klar 06/12-2019
*   Författare - Rickard Doverfelt, Wilhelm Ågren
*
*   YOU CAN DO THIS! I BELIEVE IN YOU!!! GET THAT JUICY HP-BREAD!!!!!!!!
*/

public class Heuristik {

  private static void sanity(ArrayList<ArrayList<Integer>> role_map, ArrayList<ArrayList<Integer>> scene_map, HashMap<Integer, HashSet<Integer>> actor_map, HashSet<Integer> taken_map, Kattio io) {
    // Print contents of the arrays, sanity
    io.println("--== [Sanity] ==--");
    io.println("  Printing role_map...");
    for (ArrayList<Integer> arrayList : role_map) {
      //if (arrayList == null) continue;
      io.println("  " + arrayList);
    }
    io.println("  Done!");
    io.println("  Printing scene_map...");
    for (ArrayList<Integer> integerArrayList : scene_map) {
      //if (integerArrayList == null) continue;
      io.println("  " + integerArrayList);
    }
    io.println("  Done!");
    io.println("  Printing actor_map...");
    for (int i = 1; i <= actor_map.size(); i++) {
      if (actor_map.get(i) == null) {
        io.println("  null");
      } else {
        io.println("  " + actor_map.get(i));
      }
    }
    io.println("  Done!");
    io.println("  Printing taken_roles...");
    io.println("  " + taken_roles);
    io.println("  Printing taken_actors...");
    io.println("  " + taken_actors);
    io.println("--== [Looks good?] ==--");
    //
    return;
  }
  private static HashMap<Integer, HashSet<Integer>> role_map;
  private static HashMap<Integer, HashSet<Integer>> scene_map;
  private static HashMap<Integer, HashSet<Integer>> actor_map;
  private static HashSet<Integer> taken_roles;
  private static HashSet<Integer> usable_roles;
  private static HashSet<Integer> taken_actors;

  public static void main(String[] args) {
    Kattio io = new Kattio(System.in, System.out);
    // n >= 1, s >= 1, k >= 2
    int n = io.getInt(); // Antalet roller
    int s = io.getInt(); // Antalet scener
    int k = io.getInt(); // Antalet skådespelare

    // List of all roles, and on the index of the role is a list of all actors that can play the role
    role_map = new HashMap<>();
    // List of all scenes, and on the index of the scene is a list of all roles that are a part of the scene
    scene_map = new HashMap<>();

    taken_roles = new HashSet<>();
    taken_actors = new HashSet<>();
    usable_roles = new HashSet<>();

    // Read in the role->actor mappings
    for(int i = 0; i < n; i++) {
      // Antalet följande siffror, alltså antalet skådisar som kan ha rollen i
      int a = io.getInt();
      HashSet<Integer> roles = new HashSet<>();
      for(int j = 0; j < a; j++) {
        roles.add(io.getInt());
      }
      role_map.put(i+1, roles);
    }

    // Read in the scene->role mappings
    for(int i = 0; i < s; i++) {
      // Antalet följande scener, alltså antalet roller som deltar i den i:te scenen
      int a = io.getInt();
      HashSet<Integer> scenes = new HashSet<>();
      for(int j = 0; j < a; j++) {
        scenes.add(io.getInt());
      }
      scene_map.put(i+1, scenes);
    }

    // List of all the actors, and the roles they have been assigned
    actor_map = new HashMap<>();
    initActorMap(k);
    initUsableRoles();

    divas();

    setActors();

    setSupers(k);

    //cleanup();

    if (args.length > 0) {
      //if (args[0].equals("sanity")) sanity(role_map, scene_map, actor_map, taken_roles, io);
    }
    //io.flush();
    printOut(io);
    io.flush();
    io.close();
  }

  private static void initUsableRoles() {
    for (int r = 0; r <= role_map.size(); r++) {
      for (HashSet<Integer> scene : scene_map.values()) {
        if (scene.contains(r)) usable_roles.add(r);
      }
    }
  }

  private static void cleanup() {
    @SuppressWarnings("unchecked")
    HashSet<Integer> taken = (HashSet<Integer>) taken_actors.clone();
    for (int act : taken) {
      boolean used = false;
      for (int r : actor_map.get(act)) {
        for (HashSet<Integer> scene : scene_map.values()) {
          if (scene.contains(r)) {
            used = true;
          }
          //System.err.printf("Scene: %s, Role: %d, Act: %d, Used: %b\n", scene, r, act, used);
        }
      }
      if (!used) {
        taken_actors.remove(act);
      }
    }
  }

  private static void printOut(Kattio io) {
    io.printf("%d\n", taken_actors.size());
    //io.printf("ActorMap size: %d\n", actor_map.size());
    //for (int i = 1; i <= actor_map.size(); i++) {
      //io.printf("i = %d\n", i);
      for (int i : taken_actors) {
        io.printf("%d %d %s\n", i, actor_map.get(i).size(), prettyString(actor_map.get(i)));
      }
    //}
  }

  private static String prettyString(HashSet<Integer> integers) {
    StringBuilder sb = new StringBuilder();
    for (int i : integers) {
      sb.append(i).append(' ');
    }
    return sb.toString().trim();
  }

  private static void setSupers(int k) {
    int cur = actor_map.size() + 1;
    for (int r = 1; r <= role_map.size() && cur < role_map.size() + k; r++) {
      if (!taken_roles.contains(r) && isUsed(r)) {
        actor_map.put(cur, new HashSet<>());
        actor_map.get(cur).add(r);
        taken_actors.add(cur);
        taken_roles.add(r);
        usable_roles.remove(r);
        cur++;
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static HashSet<Integer> getUsableRoles() {
    return (HashSet<Integer>) usable_roles.clone();
  }

  private static boolean isUsed(int r) {
    boolean used = false;
    for (HashSet<Integer> scene : scene_map.values()) {
      if (scene.contains(r)) {
        used = true;
        break;
      }
    }
    return used;
  }

  private static void setActors() {
    for (int a = 1; a <= actor_map.size(); a++) {
      if (!taken_actors.contains(a)) {
        for (int r = 1; r <= role_map.size(); r++) {
          if (isNormalValid(a, r)) {
            actor_map.get(a).add(r);
            taken_actors.add(a);
            taken_roles.add(r);
            usable_roles.remove(r);
          }
        }
      }
    }
  }

  private static boolean isNormalValid(int a, int r) {
    if (taken_roles.contains(r)) return false;
    if (!role_map.get(r).contains(a)) return false;
    for (int r2 : actor_map.get(a)) {
      for (HashSet<Integer> scene : scene_map.values()) {
        if (scene.contains(r) && scene.contains(r2)) return false;
      }
    }
    return true;
  }

  private static void initActorMap(int n) {
    for (int i = 1; i <= n; i++) {
      actor_map.put(i, new HashSet<>());  
      actor_map.get(i).add(-1);
      actor_map.get(i).remove(-1);
    }
  }

  private static void divas() {
    HashSet<Integer> d1 = new HashSet<>(), d2 = new HashSet<>();
    for (int r : usable_roles) {
      if (role_map.get(r).contains(1)) {
        d1.add(r);
      }
      if (role_map.get(r).contains(2)) {
        d2.add(r);
      }
    }

    for (int r1 : d1) {
      for (int r2 : d2) {
        if (isDivaValid(r1, r2)) {
          actor_map.get(1).add(r1);
          actor_map.get(2).add(r2);
          taken_roles.add(r1);
          taken_roles.add(r2);
          taken_actors.add(2);
          taken_actors.add(1);
          return;
        }
      }
    }
  }

  private static boolean isDivaValid(int r1, int r2) {
    if (r1 == r2) return false;
    boolean valid = true;
    boolean r1found = false, r2found = false;
    for (HashSet<Integer> scene : scene_map.values()) {
      if (scene.contains(r1) && scene.contains(r2)) valid = false;
      if (scene.contains(r1)) r1found = true;
      if (scene.contains(r2)) r2found = true;
    }
    return valid && r1found && r2found;
  }
}